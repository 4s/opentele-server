package org.opentele.server.saml

import javax.crypto.Cipher
import javax.servlet.Filter;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager
import org.codehaus.groovy.grails.plugins.springsecurity.AjaxAwareAuthenticationFailureHandler
import org.codehaus.groovy.grails.plugins.springsecurity.SecurityFilterPosition
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import org.opensaml.saml2.metadata.provider.FilesystemMetadataProvider
import org.opensaml.saml2.metadata.provider.HTTPMetadataProvider
import org.opensaml.xml.parse.StaticBasicParserPool
import org.opentele.server.util.ConfigUtil
import org.opentele.taglib.SamlTagLib
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.FileSystemResource
import org.springframework.security.saml.SAMLAuthenticationProvider
import org.springframework.security.saml.SAMLBootstrap
import org.springframework.security.saml.SAMLEntryPoint
import org.springframework.security.saml.SAMLLogoutFilter
import org.springframework.security.saml.SAMLLogoutProcessingFilter
import org.springframework.security.saml.SAMLProcessingFilter
import org.springframework.security.saml.context.SAMLContextProviderImpl
import org.springframework.security.saml.key.JKSKeyManager
import org.springframework.security.saml.log.SAMLDefaultLogger
import org.springframework.security.saml.metadata.CachingMetadataManager
import org.springframework.security.saml.metadata.ExtendedMetadataDelegate
import org.springframework.security.saml.metadata.MetadataDisplayFilter
import org.springframework.security.saml.metadata.MetadataGenerator
import org.springframework.security.saml.metadata.MetadataGeneratorFilter
import org.springframework.security.saml.parser.ParserPoolHolder
import org.springframework.security.saml.processor.HTTPArtifactBinding
import org.springframework.security.saml.processor.HTTPPAOS11Binding
import org.springframework.security.saml.processor.HTTPPostBinding
import org.springframework.security.saml.processor.HTTPRedirectDeflateBinding
import org.springframework.security.saml.processor.HTTPSOAP11Binding
import org.springframework.security.saml.processor.SAMLProcessorImpl
import org.springframework.security.saml.util.VelocityFactory
import org.springframework.security.saml.websso.ArtifactResolutionProfileImpl
import org.springframework.security.saml.websso.SingleLogoutProfileImpl
import org.springframework.security.saml.websso.WebSSOProfileConsumerImpl
import org.springframework.security.saml.websso.WebSSOProfileECPImpl
import org.springframework.security.saml.websso.WebSSOProfileImpl
import org.springframework.security.saml.websso.WebSSOProfileOptions
import org.springframework.security.web.DefaultRedirectStrategy
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler
import org.springframework.security.web.authentication.session.SessionFixationProtectionStrategy
import org.springframework.security.web.FilterChainProxy

class SamlSpringConfigUtils {

  static boolean getIsSamlActive() {
    // handling string version of true and false (external properties are always strings)
    return ConfigUtil.toBoolean(SpringSecurityUtils.securityConfig.saml.active)
  }

  private static def getResource(path) {
    path.startsWith("classpath:") ? new ClassPathResource(path.substring(10)) : new FileSystemResource(path)
  }

  // TODO: BEGIN GRAILS SPRING SECURITY CORE FIX
  // see https://jira.grails.org/browse/GPSPRINGSECURITYCORE-210
  // notice the grailsApplication assigned in BootStrap can be removed at upgrade to 2.4
  static def grailsApplication

  private static void clientRegisterFilterFix(final String beanName, final int order) {
    def positionToFilter = SpringSecurityUtils.getConfiguredOrderedFilters()
    def filterToPosition = [:]
    positionToFilter.each {position, filter ->
      filterToPosition[filter] = position
    }

    Filter oldFilter = positionToFilter.get(order);
    if (oldFilter != null) {
      throw new IllegalArgumentException("Cannot register filter '" + beanName +
        "' at position " + order + "; '" + oldFilter +
        "' is already registered in that position");
    }

    Filter filter = (Filter)grailsApplication.mainContext.getBean(beanName);
    positionToFilter.put(order, filter);
    FilterChainProxy filterChain = (FilterChainProxy)grailsApplication.mainContext.getBean("springSecurityFilterChain");
    def filterChainMap = filterChain.getFilterChainMap()
    def correctFilterChainMap = filterChainMap.collectEntries { pattern, filters ->
      def indexOfFilterBeforeTargetFilter = 0
      while(indexOfFilterBeforeTargetFilter < filters.size() && filterToPosition[filters[indexOfFilterBeforeTargetFilter]] < order) {
        indexOfFilterBeforeTargetFilter++
      }
      filters.add(indexOfFilterBeforeTargetFilter, filter)

      [pattern, filters]
    }
    filterChain.filterChainMap = Collections.unmodifiableMap(correctFilterChainMap)
  }
  // END GRAILS SPRING SECURITY CORE FIX

  static registerFilters = {
    if (!isSamlActive) return
    log.debug "Bootstrapping SAML filters"
    def registerFilter = SamlSpringConfigUtils.&clientRegisterFilterFix
    // TODO: change to the following after upgrade to Grails 2.4
    // def registerFilter = SpringSecurityUtils.&clientRegisterFilter
    int i = 1
    registerFilter 'samlEntryPoint', SecurityFilterPosition.SECURITY_CONTEXT_FILTER.order + i++
    registerFilter 'metadataGeneratorFilter', SecurityFilterPosition.SECURITY_CONTEXT_FILTER.order + i++
    registerFilter 'metadataDisplayFilter', SecurityFilterPosition.SECURITY_CONTEXT_FILTER.order + i++
    registerFilter 'samlProcessingFilter', SecurityFilterPosition.SECURITY_CONTEXT_FILTER.order + i++
    registerFilter 'samlLogoutFilter', SecurityFilterPosition.SECURITY_CONTEXT_FILTER.order + i++
    registerFilter 'samlLogoutProcessingFilter', SecurityFilterPosition.SECURITY_CONTEXT_FILTER.order + i++
    // registerFilter 'samlIdpDiscovery', SecurityFilterPosition.SECURITY_CONTEXT_FILTER.order + i++
  }

  static createBeans = {
    def conf = SpringSecurityUtils.securityConfig

    log.debug('Configuring Spring Security SAML ...')

    if (isSamlActive) {
      if (Cipher.getMaxAllowedKeyLength("SHA") < 256) {
        log.error("Java Cryptography Extension (JCE) must be installed to support SHA256")
      }
    }

    //Due to Spring DSL limitations, need to import these beans as XML definitions
    def beansPath = "classpath:security/springSecuritySamlBeans.xml"
    delegate.importBeans beansPath

    xmlns context: "http://www.springframework.org/schema/context"
    context.'annotation-config'()
    context.'component-scan'('base-package': "org.springframework.security.saml")

    SpringSecurityUtils.registerProvider 'samlAuthenticationProvider'
    SpringSecurityUtils.registerLogoutHandler 'successLogoutHandler'
    SpringSecurityUtils.registerLogoutHandler 'logoutHandler'

    successRedirectHandler(SavedRequestAwareAuthenticationSuccessHandler) {
      // alwaysUseDefaultTargetUrl = conf.saml.alwaysUseAfterLoginUrl ?: false
      // defaultTargetUrl = conf.saml.afterLoginUrl
    }

    successLogoutHandler(SimpleUrlLogoutSuccessHandler) {
      // defaultTargetUrl = conf.saml.afterLogoutUrl
    }

    samlLogger(SAMLDefaultLogger)

    keyManager(JKSKeyManager) { bean ->
      bean.constructorArgs = [
        getResource(conf.saml.keyManager.storeFile),
        conf.saml.keyManager.storePass,
        ConfigUtil.toMap(conf.saml.keyManager.passwords),
        conf.saml.keyManager.defaultKey
      ]
    }

    samlEntryPoint(SAMLEntryPoint) {
      defaultProfileOptions = ref('webProfileOptions')
    }

    webProfileOptions(WebSSOProfileOptions) {
      includeScoping = false
    }

    metadataDisplayFilter(MetadataDisplayFilter)

    metadataGenerator(MetadataGenerator) {
      if (conf.saml.metadata.sp.entityId) entityId = conf.saml.metadata.sp.entityId
      if (conf.saml.metadata.sp.entityBaseUrl) entityBaseURL = conf.saml.metadata.sp.entityBaseUrl
    }

    metadataGeneratorFilter(MetadataGeneratorFilter, ref('metadataGenerator'))

    parserPool(StaticBasicParserPool) { bean ->
      bean.initMethod = 'initialize'
    }

    parserPoolHolder(ParserPoolHolder) { bean ->
      bean.autowire = 'byName'
    }

    def idpUrl = conf.saml.metadata.idp.metadataUrl
    def idpFile = conf.saml.metadata.idp.file
    if (idpUrl) {
      log.debug("Defining IdP metadata provider from url " + idpUrl)
      def timer = new Timer(true)
      idpMetadata(ExtendedMetadataDelegate) { extMetaDataDelegateBean ->
        idpMetadataProvider(HTTPMetadataProvider) { bean ->
          bean.constructorArgs = [timer, ref('httpClient') , idpUrl]
          parserPool = ref('parserPool')
        }
        extMetaDataDelegateBean.constructorArgs = [ref('idpMetadataProvider')]
      }
    }
    else if (idpFile) {
      def idpResource = getResource(idpFile)
      log.debug("Defining IdP metadata provider from file " + idpFile)
      idpMetadata(ExtendedMetadataDelegate) { extMetaDataDelegateBean ->
        idpMetadataProvider(FilesystemMetadataProvider) { bean ->
          bean.constructorArgs = [idpResource.getFile()]
          parserPool = ref('parserPool')
        }
        extMetaDataDelegateBean.constructorArgs = [ref('idpMetadataProvider')]
        // metadataTrustCheck = !conf.saml.metadata.idp.metadataTrustCheck
      }
    }
    else {
      log.error("Missing IdP metadata provider specification")
    }

    // TODO: The list of providers cannot be given in the constructor in Grails 2.1.
    // Passing null and assigning the property. Can be changed in Grails 2.4
    metadata(CachingMetadataManager, null) {
      providers = [ref('idpMetadata')]
    }

    samlClinicianDetailsService(SamlClinicianDetailsService) {
      grailsApplication = ref('grailsApplication')
      clinicianService = ref('clinicianService')
      passwordService = ref('passwordService')
      samlUserAttributes = conf.saml.userAttributes
      ignorePermissions = ConfigUtil.toList(conf.saml.ignorePermissions)
    }

    samlAuthenticationProvider(SAMLAuthenticationProvider) {
      userDetails = ref('samlClinicianDetailsService')
      hokConsumer = ref('webSSOprofileConsumer')
      forcePrincipalAsString = false
    }

    contextProvider(SAMLContextProviderImpl)

    samlProcessingFilter(SAMLProcessingFilter) {
      authenticationManager = ref('authenticationManager')
      authenticationSuccessHandler = ref('successRedirectHandler')
      sessionAuthenticationStrategy = ref('sessionFixationProtectionStrategy')
      authenticationFailureHandler = ref('authenticationFailureHandler')
    }

    authenticationFailureHandler(AjaxAwareAuthenticationFailureHandler) {
      redirectStrategy = ref('redirectStrategy')
      defaultFailureUrl = conf.failureHandler.defaultFailureUrl //'/login/authfail?login_error=1'
      useForward = conf.failureHandler.useForward // false
      ajaxAuthenticationFailureUrl = conf.failureHandler.ajaxAuthFailUrl // '/login/authfail?ajax=true'
      exceptionMappings = conf.failureHandler.exceptionMappings // [:]
    }

    redirectStrategy(DefaultRedirectStrategy) {
      contextRelative = conf.redirectStrategy.contextRelative // false
    }

    sessionFixationProtectionStrategy(SessionFixationProtectionStrategy)

    logoutHandler(SecurityContextLogoutHandler) {
      invalidateHttpSession = true
    }

    samlLogoutFilter(SAMLLogoutFilter,
        ref('successLogoutHandler'), ref('logoutHandler'), ref('logoutHandler'))

    samlLogoutProcessingFilter(SAMLLogoutProcessingFilter,
        ref('successLogoutHandler'), ref('logoutHandler'))

    webSSOprofileConsumer(WebSSOProfileConsumerImpl) {
      responseSkew = ConfigUtil.toInteger(conf.saml.responseSkew)
      maxAuthenticationAge = ConfigUtil.toInteger(conf.saml.maxAuthenticationAge)
    }

    webSSOprofile(WebSSOProfileImpl)

    ecpprofile(WebSSOProfileECPImpl)

    logoutprofile(SingleLogoutProfileImpl)

    postBinding(HTTPPostBinding, ref('parserPool'), ref('velocityEngine'))

    redirectBinding(HTTPRedirectDeflateBinding, ref('parserPool'))

    artifactBinding(HTTPArtifactBinding,
        ref('parserPool'),
        ref('velocityEngine'),
        ref('artifactResolutionProfile')
    )

    artifactResolutionProfile(ArtifactResolutionProfileImpl, ref('httpClient')) {
      processor = ref('soapProcessor')
    }

    multiThreadedHttpConnectionManager(MultiThreadedHttpConnectionManager)

    httpClient(HttpClient, ref('multiThreadedHttpConnectionManager'))

    soapProcessor(SAMLProcessorImpl, ref('soapBinding'))

    soapBinding(HTTPSOAP11Binding, ref('parserPool'))

    paosBinding(HTTPPAOS11Binding, ref('parserPool'))

    if (ConfigUtil.toBoolean(conf.saml.metadata.idp.sha256fix)) {
      bootStrap(SamlBootstrapSha256)
    }
    else {
      bootStrap(SAMLBootstrap)
    }

    velocityEngine(VelocityFactory) { bean ->
      bean.factoryMethod = "getEngine"
    }

    securityTagLib(SamlTagLib) {
      springSecurityService = ref('samlSecurityService')
      webExpressionHandler = ref('webExpressionHandler')
      webInvocationPrivilegeEvaluator = ref('webInvocationPrivilegeEvaluator')
    }

    samlSecurityService(SamlSecurityService) {
      grailsApplication = ref('grailsApplication')
      authenticationTrustResolver = ref('authenticationTrustResolver')
      passwordEncoder = ref('passwordEncoder')
      objectDefinitionSource = ref('objectDefinitionSource')
      userDetailsService = ref('samlClinicianDetailsService')
      userCache = ref('userCache')
    }

    log.debug('... finished configuring Spring Security SAML')
  }
}
