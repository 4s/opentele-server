package org.opentele.server.saml

import org.codehaus.groovy.grails.plugins.springsecurity.GormUserDetailsService
import org.opensaml.xml.schema.XSAny
import org.opensaml.xml.schema.XSString
import org.opentele.server.model.ClinicianCommand
import org.opentele.server.model.Clinician
import org.opentele.server.model.OpenteleGrailsUserDetails
import org.opentele.server.model.User
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.saml.SAMLCredential
import org.springframework.security.saml.userdetails.SAMLUserDetailsService

class SamlClinicianDetailsService extends GormUserDetailsService implements SAMLUserDetailsService {

  def clinicianService
  def passwordService
  Map samlUserAttributes
  List ignorePermissions

  public Object loadUserBySAML(SAMLCredential credential) throws UsernameNotFoundException {
    User.withTransaction { status ->
      def command = new ClinicianCommand()

      command.isSamlShadow = true
      command.username = getSamlUsername(credential)
      command.firstName = getSamlAttribute(credential, 'firstName')
      command.lastName = getSamlAttribute(credential, 'lastName')
      command.email = getSamlAttribute(credential, 'email')
      command.phone = getSamlAttribute(credential, 'phone')
      command.mobilePhone = getSamlAttribute(credential, 'mobilePhone')
      command.videoUser = getSamlAttribute(credential, 'videoUser')
      command.videoPassword = getSamlAttribute(credential, 'videoPassword')

      // add roles and groups with case insensitive match using already fetched lists
      def roleNames = getSamlAttributes(credential, "roleName")
      roleNames.each { name ->
        def role = command.possibleRoles.find{it.authority.equalsIgnoreCase(name)}
        if (role) {
          command.roleIds << role.id
        }
        else {
          log.warn("SAML claimed role ${name} is not used")
        }
      }

      def groupNames = getSamlAttributes(credential, "patientGroupName")
      groupNames.each { name ->
        def group = command.possiblePatientGroups.find{it.name.equalsIgnoreCase(name)}
        if (group) {
          command.groupIds << group.id
        }
        else {
          log.warn("SAML claimed patient group ${name} is not used")
        }
      }

      // create or fetch and update user
      Clinician clinician
      User user = User.findByUsername(command.username)
      if (user) clinician = Clinician.findByUser(user)
      if (clinician) {
        command.id = clinician.id
        command.version = clinician.version
        command.cleartextPassword = user.cleartextPassword
        clinician = clinicianService.update(command, clinician)
      } else {
        command.cleartextPassword = passwordService.generateTempPassword()
        clinician = clinicianService.createClinician(command)
      }
      // get the user again in case it was created or updated
      user = clinician.user

      def samlGrantedAuthorities = user.grantedAuthorities.findAll{ !ignorePermissions.contains(it.authority) }
      // TODO: why not use GrailsUser directly?
      return new OpenteleGrailsUserDetails(user.username, user.password, user.enabled,
        !user.accountExpired, !user.passwordExpired, !user.accountLocked, samlGrantedAuthorities, user.id)
    }
  }

  protected String getSamlUsername(credential) {
    if (samlUserAttributes?.username) {
      def attribute = credential.getAttribute(samlUserAttributes.username)
      def value = attribute?.attributeValues?.value
      return value?.first()
    }
    else {
      // if no mapping provided for username attribute then assume it is the returned subject in the assertion
      return credential.nameID?.value
    }
  }

  protected List<String> getSamlAttributes(credential, key) {
    if (!samlUserAttributes.containsKey(key)) return []

    key = samlUserAttributes.get(key)
    def attribute = credential.getAttribute(key)
    def values = attribute?.attributeValues
    if (!values) return []
    if (values[0] instanceof XSAny) return values*.textContent
    if (values[0] instanceof XSString) return values*.value
    return values*.toString()
  }

  protected String getSamlAttribute(credential, key) {
    if (!samlUserAttributes.containsKey(key)) return null

    key = samlUserAttributes.get(key)
    def attribute = credential.getAttribute(key)
    def value = attribute?.attributeValues?.first()
    if (!value) return null
    if (value instanceof XSAny) return value.textContent
    if (value instanceof XSString) return value.value
    return value.toString()
  }
}
