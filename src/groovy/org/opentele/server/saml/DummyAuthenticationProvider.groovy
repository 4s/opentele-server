package org.opentele.server.saml

import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException

class DummyAuthenticationProvider implements AuthenticationProvider {

  Authentication authenticate(Authentication var1) throws AuthenticationException {
    return null
  }

  boolean supports(Class<? extends Object> var1) {
    return false;
  }
}
