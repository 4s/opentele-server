package org.opentele.server.util

class ConfigUtil {

  static def toBoolean(x) {
    if (x instanceof String) return x.toBoolean()
    return x
  }

  static def toInteger(x) {
    if (x instanceof String) return x.toInteger()
    return x
  }

  static def toDouble(x) {
    if (x instanceof String) return x.toDouble()
    return x
  }

  static def toList(x) {
    if (x instanceof String) return Eval.me(x)
    return x
  }

  static def toMap(x) {
    if (x instanceof String) return Eval.me(x)
    return x
  }
}
