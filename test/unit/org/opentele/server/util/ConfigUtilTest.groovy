package org.opentele.server.util

import org.junit.*
import org.opentele.server.util.ConfigUtil

class ConfigUtilTest {

  @Test
  void testToBoolean() {
    assert ConfigUtil.toBoolean(false) == false
    assert ConfigUtil.toBoolean("false") == false
    assert ConfigUtil.toBoolean(true) == true
    assert ConfigUtil.toBoolean("true") == true
  }

  @Test
  void testToInteger() {
    assert ConfigUtil.toInteger(7) == 7
    assert ConfigUtil.toInteger("7") == 7
  }

  @Test
  void testToDouble() {
    assert ConfigUtil.toDouble(3.14) == 3.14
    assert ConfigUtil.toDouble("3.14") == 3.14
  }

  @Test
  void testToList() {
    assert ConfigUtil.toList([]) == []
    assert ConfigUtil.toList("[]") == []
    assert ConfigUtil.toList(['a','b']) == ['a','b']
    assert ConfigUtil.toList("['a','b']") == ['a','b']
  }

  @Test
  void testToMap() {
    assert ConfigUtil.toMap([:]) == [:]
    assert ConfigUtil.toMap("[:]") == [:]
    assert ConfigUtil.toMap([a:'a',b:'b']) == [a:'a',b:'b']
    assert ConfigUtil.toMap("[a:'a',b:'b']") == [a:'a',b:'b']
  }
}
