CREATE TABLE [dbo].[help_image] (
  [id] BIGINT IDENTITY NOT NULL,
  [version] BIGINT NOT NULL,
  [filename] NVARCHAR(512),
  CONSTRAINT [help_imagePK] PRIMARY KEY (id)
)
GO

CREATE TABLE [dbo].[help_info] (
  [id] BIGINT IDENTITY NOT NULL,
  [version] BIGINT NOT NULL,
  [help_image_id] BIGINT,
  [text] NVARCHAR(max),
  CONSTRAINT [help_infoPK] PRIMARY KEY (id)
)
GO

Create TABLE [dbo].[patient_help_info] (
  [id] BIGINT IDENTITY NOT NULL,
  [version] BIGINT NOT NULL,
  [help_image_id] BIGINT,
  [text] NVARCHAR(max),
  CONSTRAINT [patient_help_PK] PRIMARY KEY (id)
)
GO

ALTER TABLE [dbo].[patient_questionnaire_node] ADD [help_info_id] BIGINT
GO

ALTER TABLE [dbo].[questionnaire_node] ADD [help_info_id] BIGINT
GO

ALTER TABLE [dbo].[help_info] ADD CONSTRAINT [FK4D4FAEECF165DE37] FOREIGN KEY ([help_image_id]) REFERENCES [dbo].[help_image] ([id])
GO

CREATE INDEX [FK4D4FAEECF165DE37] ON [dbo].[help_info]([help_image_id])
GO

ALTER TABLE [dbo].[patient_questionnaire_node] ADD CONSTRAINT [FKC44D1298E4CDFFB2] FOREIGN KEY ([help_info_id]) REFERENCES [dbo].[patient_help_info] ([id])
GO

CREATE INDEX [FKC44D1298E4CDFFB2] ON [dbo].[patient_questionnaire_node]([help_info_id])
GO

ALTER TABLE [dbo].[questionnaire_node] ADD CONSTRAINT [FK7BD3671E719CA8E8] FOREIGN KEY ([help_info_id]) REFERENCES [dbo].[help_info] ([id])
GO

CREATE INDEX [FK7BD3671E719CA8E8] ON [dbo].[questionnaire_node]([help_info_id])
GO
