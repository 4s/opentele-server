databaseChangeLog = {

	changeSet(author: "tbh", id: "saml-1") {
		addColumn(tableName: "users") {
			column(name: "is_saml_shadow", type: "boolean") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "tbh", id: "saml-2") {
		modifyDataType(columnName: "PASSWORD", newDataType: "varchar(512)", tableName: "USERS")
	}

	changeSet(author: "tbh", id: "saml-3") {
		dropNotNullConstraint(columnDataType: "varchar(512)", columnName: "PASSWORD", tableName: "USERS")
	}
}
