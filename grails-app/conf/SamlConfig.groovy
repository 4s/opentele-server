import org.opentele.server.model.types.PermissionName

grails { plugins { springsecurity {
  saml {
    // Determines whether SAML is the clinician authentication method
    active = false

    // Defines the mapping for user attributes
    userAttributes {
      firstName = 'FirstName'
      lastName = 'LastName'
      email = 'EmailAddress'
      // phone = ''
      // mobilePhone = ''
      roleName = 'LastName' // hack to use last name as the role
      // patientGroupName = ''
      // videoUser = ''
      // videoPassword = ''
    }

    // Permissions to ignore for the clinician
    ignorePermissions = [
        PermissionName.CLINICIAN_CHANGE_PASSWORD,
        PermissionName.CLINICIAN_WRITE,
        PermissionName.CLINICIAN_CREATE
    ]

    // Set loginFormUrl to /saml/login to go directly to IdP login page
    // loginFormUrl = /saml/login

    // Time tolerance in seconds (time skew) between SP and IdP
    responseSkew = 60

    // Max authentication age in seconds
    maxAuthenticationAge = 28800 // 8 hours

    metadata {
      idp {
        // Setting the title will show it on the login page
        title = "SSOCircle"

        // The idp metadata. Either as a local file or a URL
        file = 'classpath:security/idp.xml'
        // metadataUrl = 'http://idp.ssocircle.com/'

        // A bug in the libraries prevents the use of SHA256, set to true to enable a fix
        sha256fix = false
      }
      sp {
        // Unique identifier of the service provider. Inherits entityBaseUrl if not set.
        // entityId = 'http://localhost:8080/opentele-server/saml/metadata'

        // SP front-end URL
        // entityBaseUrl = 'http://localhost:8080/opentele-server/saml/metadata'
      }
    }
    keyManager {
      // Path to the keystore file
      storeFile = 'classpath:security/keystore.jks'

      // Password to the keystore
      storePass = 'rhaisb42'

      // Passwords to the  keys
      passwords = [ opentele: 'rhaisb42' ]

      // Default key
      defaultKey = 'opentele'
    }
  }
}}}
