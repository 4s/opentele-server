package org.opentele.server.saml

import grails.plugins.springsecurity.SpringSecurityService
import org.codehaus.groovy.grails.plugins.springsecurity.GrailsUser
import org.opentele.server.model.User

class SamlSecurityService extends SpringSecurityService {
  static transactional = false

  Object getCurrentUser() {
    if (!isLoggedIn()) return null
    if (SamlSpringConfigUtils.isSamlActive) {
      // lookup user by username and not id as in the super class
      def userDetails = getAuthentication().details
      if (userDetails instanceof GrailsUser) return User.findByUsername(userDetails.username)
    }
    return super.getCurrentUser()
  }
}
