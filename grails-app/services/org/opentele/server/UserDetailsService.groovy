package org.opentele.server

import org.codehaus.groovy.grails.plugins.springsecurity.GrailsUserDetailsService
import org.opentele.server.model.OpenteleGrailsUserDetails
import org.opentele.server.model.User
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.dao.DataAccessException
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException

/**
 * Implement UserDetails service to handle roles and permissions
 */
class UserDetailsService implements GrailsUserDetailsService {
    private static final Logger log = LoggerFactory.getLogger(UserDetailsService.class.name)

    @Override
    UserDetails loadUserByUsername(String username, boolean loadRoles) throws UsernameNotFoundException, DataAccessException {
        return loadUserByUsername(username)
    }

    @Override
    UserDetails loadUserByUsername(String s) throws UsernameNotFoundException, DataAccessException {
        User.withTransaction { status ->
            User user = User.findByUsername(s)
            if (!user) {
                throw new UsernameNotFoundException('User not found', s)
            }

            return new OpenteleGrailsUserDetails(user.username, user.password, user.enabled,
                    !user.accountExpired, !user.passwordExpired,
                    !user.accountLocked, user.grantedAuthorities, user.id)
        }
    }
}
