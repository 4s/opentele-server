package org.opentele.server.model

class HelpImage extends AbstractObject{
    String filename

    static constraints = {
        filename(blank:false,nullable:false)
    }
}
