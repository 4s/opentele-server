package org.opentele.server.model

import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import org.opentele.server.util.PasswordUtil
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.GrantedAuthorityImpl
import org.springframework.security.core.context.SecurityContextHolder

class User extends AbstractObject {

	transient springSecurityService

	String username
	String password
	boolean enabled
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired
    String cleartextPassword
    int badLoginAttemps
	boolean isSamlShadow = false

	def name() {
		def patient = Patient.findByUser(this)
		if (patient) {
			return patient.toString()
		}
				
		def clinician = Clinician.findByUser(this)
		if (clinician) {
			return clinician.toString()
		}
		
		return username
	}
	
	def isPatient() {
        Patient.findByUser(this) != null
    }

    def isClinician() {
        Clinician.findByUser(this) != null
    }

    static constraints = {
        username nullable: false, unique: true, blank: false, maxSize:  128
        password nullable: false, blank: false, validator: PasswordUtil.passwordValidator
        cleartextPassword nullable: true, blank: true, validator: PasswordUtil.cleartextPasswordValidator
	}

	static mapping = {
		password column: 'password'
        table 'users'
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role } as Set
	}

	Set<GrantedAuthority> getGrantedAuthorities() {
		def result = []
		this.authorities.each {
			def perms = RolePermission.findAllByRole(it)
			perms.each { perm ->
				result << new GrantedAuthorityImpl(perm.permission.permission)
			}
		}
		return result ?: [new GrantedAuthorityImpl(SpringSecurityUtils.NO_ROLE)]
	}

	def beforeInsert() {

        def createdDate =  new Date()

        this.createdDate = createdDate
        this.createdBy = this.modifiedByUser
        this.modifiedDate = createdDate
        this.modifiedBy = this.modifiedByUser

		encodePassword()
	}

	def beforeUpdate() {

        this.modifiedBy = this.modifiedByUser
        this.modifiedDate = new Date()

        if (!password || password.empty) {
            return; //Don't encode empty passwords. They will cause the constraint password: 'blank:false' not to fail
        }
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		if (password) {
			password = springSecurityService.encodePassword(password.toLowerCase())
		}
	}

	private def getModifiedByUser() {
		(this.isSamlShadow) ? "SAML" : SecurityContextHolder.context.authentication?.name ?: "Unknown"
	}
}
